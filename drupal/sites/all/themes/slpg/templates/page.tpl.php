<?php
//kpr(get_defined_vars());
//kpr($theme_hook_suggestions);
//template naming
//page--[CONTENT TYPE].tpl.php
?>
<?php if( theme_get_setting('mothership_poorthemers_helper') ){ ?>
<!--page.tpl.php-->
<?php } ?>

<?php print $mothership_poorthemers_helper; ?>

<div id="wrapper">
  <header id="header" role="banner">
    <div class="container">
      <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" id="site-logo" title="<?php print t('Home'); ?>" rel="home">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>

      <div id="menu">
        <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('id' => 'main-menu'))); ?>
      </div>
    </div>
  </header>

  <div id="page-container" class="container">
    <div class="page light shadowed">

      <div id="sidebar" class="block width-4 darken">
        <div class="clarity">
          <?php if ($page['sidebar_first']): ?>
            <div class="sidebar-first">
            <?php print render($page['sidebar_first']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>

      <div id="main-content" class="block width-8" role="main">
        <div class="clarity">
          <?php print render($title_prefix); ?>
          <?php if ($title): ?>
            <h1><?php print $title; ?></h1>
          <?php endif; ?>
          <?php print render($title_suffix); ?>

          <?php print render($page['content_pre']); ?>

          <?php print render($page['content']); ?>

          <?php print render($page['content_post']); ?>
        </div>
      </div><!--/main-->
      <div class="clear"></div>
    </div>
  </div><!--/page-container-->

  <footer id="footer" class="darken" role="contentinfo">
    <div class="container">
      <div class="block width-4">
        <div>
          <div class="clarity" style="line-height: 2em">
            <h3>Menu</h3>
            <div class="width-50perc">
              <ul>
                <li><a href="">Início</a></li><!--
             --><li><a href="oescritorio">O Escritório</a></li><!--
             --><li><a href="equipe">Equipe</a></li><!--
             --><li><a href="noticias">Notícias</a></li>
              </ul>
            </div>
            <div class="width-50perc">
              <ul>
                <li><a href="publicacoes">Publicações</a></li><!--
             --><li><a href="links">Links</a></li><!--
             --><li><a href="contato">Contato</a></li><!--
             --><li><a href="user/login">Administração</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="block width-4">
          <div>
              <div class="clarity">
                  <h3>Endereço</h3>
                  <div class="info">
                      Rua Nunes Machado, 94 - 9º andar, Centro<br />
                      Florianópolis - Santa Catarina
                  </div>
                  <div class="info">
                      Telefone/FAX: <span style="color: #e1e2e4">(48) 3024-4166</span>
                  </div>
              </div>
          </div>
      </div>
      <div class="block width-4">
          <div>
              <div class="clarity less-top text-to-right">
                  <a href="http://cnasp.adv.br/">
                      <img id="cnasp" src="<?php echo drupal_get_path('theme', 'slpg'); ?>/images/temp/cnasp.png" title="CNASP - Coletivo Nacional de Advogados de Servidores Públicos" />
                  </a>
              </div>
          </div>
      </div>
      <div class="clear"></div>
    </div>
  </footer>
</div>

